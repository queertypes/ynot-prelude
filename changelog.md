# 0.4.2.0 (April 9, 2019)

* fix: broken version bounds
  - containers
  - profunctors
  - stm

# 0.4.1.0 (October 19, 2018)

* fix: broken version bounds
  - profunctors
  - stm
  - time

# 0.4.0.0 (October 6, 2018)

* remove: control-monad-free
* remove: lens
* remove: package.yaml
* add: cabal.project
* add: ynot-prelude.cabal

Generally, a clean-up release. Also, an intention to move away from
requiring stack, and instead allowing for cabal and its newer
facilities to be used.

# 0.3.0.0 (January 11, 2018)

* remove: Data.Text.Reader
  * was colliding with attoparsec defs

# 0.2.0.0 (December 5, 2017)

* license: BSD3 -> GPL-3

* remove:
  * adjunctions
  * bifunctors
  * clock
  * comonad
  * contravariant
  * distributive
  * kan-extensions
  * pointed
  * semigroupoids
  * semigroups
  * void
  * mtl
  * GHC generics imports

* add:
  * uuid

* cleanup:
  * Control.Concurrent imports

* improve:
  * time imports (bring time types into scope)
  * vector import (bring MVector in)
  * simplify Print

# 0.1.8.0 (April 27, 2017)

* remove: semiring-simple

# 0.1.7.0 (April 27, 2017)

* expose: mconcat, mempty from `Data.Monoid`

# 0.1.6.0 (April 23, 2017)

* **actually** import IO and Time modules

# 0.1.5.1 (April 23, 2017)

* bring in more of Control.Lens.Review
    * fixes a bug in using this for `stack ghci`

# 0.1.5.0 (April 23, 2017)

* add hackage:path
* expose FilePath type

# 0.1.4.0 (March 30, 2017)

* add in missing, common typeclasses:
  * Data.Ord
  * Data.Eq
  * GHC.Enum

# 0.1.3.2 (March 29, 2017)

* minor stylistic changes

# 0.1.3.1 (January 16, 2017)

* exports: Data.Bifunctor, ($), curry, uncurry

# 0.1.3.0 (January 9, 2017)

* expose Control.Lens.TH
* expose Data.List.NonEmpty type
