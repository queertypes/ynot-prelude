{-# OPTIONS_GHC -Wall #-}
module Data (
  module X,
  LText,
  LByteString,
  BytestringBuilder
) where

import Data.Array as X (Array)
import Data.Bifunctor as X
import Data.Bits as X
import Data.Bool as X hiding (bool)
import Data.ByteString as X (ByteString)
import Data.Char as X
import Data.Eq as X
import Data.Ord as X
import Data.DList as X (DList)
import Data.Either as X
import Data.Foldable as X hiding (
  foldr1
  , foldl1
  , maximum
  , maximumBy
  , minimum
  , minimumBy
  )

import Data.Function as X (
  const
  , (.)
  , flip
  , fix
  , on
  , ($)
  )

import Data.HashMap.Strict as X (HashMap)
import Data.HashSet as X (HashSet)
import Data.Hashable as X
import Data.Int as X
import Data.IntMap as X (IntMap)
import Data.IntSet as X (IntSet)
import Data.List as X (
    splitAt
  , break
  , intercalate
  , isPrefixOf
  , drop
  , filter
  , reverse
  , replicate
  )

import Data.List.NonEmpty as X (NonEmpty)
import Data.Map as X (Map)
import Data.Maybe as X hiding (fromJust)
import Data.Monoid as X (Monoid(..), mempty, mconcat)
import Data.Proxy as X (Proxy)
import Data.Semigroup as X (Semigroup(..))
import Data.Sequence as X (Seq)
import Data.Set as X (Set)
import Data.STRef as X (STRef)
import Data.Tagged as X
import Data.Text as X (Text)
import Data.Text.Lazy as X (toStrict, fromStrict)
import Data.Time as X (
  UTCTime,
  NominalDiffTime,
  Day,
  TimeOfDay,
  TimeZone,
  LocalTime,
  ZonedTime
  )

import Data.Time.Clock.POSIX as X (POSIXTime)

import Text.Printf as X (PrintfArg, printf, hPrintf)
import Data.UUID as X (UUID)
import Data.Vector as X (Vector)
import Data.Vector.Mutable as X (MVector)
import Data.Void as X
import Data.Word as X

import qualified Data.ByteString.Lazy as LB
import qualified Data.Text.Lazy as LT
import qualified Data.ByteString.Builder as BB

type LText = LT.Text
type LByteString = LB.ByteString
type BytestringBuilder = BB.Builder
