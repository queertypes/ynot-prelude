{-# OPTIONS_GHC -Wall #-}
module Control (
  module X,
  liftM',
  liftM2'
) where

-- base.Applicative: pure, (<*>), (*>), (<*)
-- base.Alternative: empty, (<|>), some, many
-- other: (<$>), (<$), (<**>)
import Control.Applicative as X

import Control.Concurrent as X (ThreadId)
import Control.Concurrent.Async as X (Async)
import Control.Concurrent.MVar as X (MVar)
import Control.DeepSeq as X
import Control.Exception as X
import Control.Monad as X

import Control.Monad.ST as X (ST)
import Control.Monad.STM as X (STM)
import Control.Concurrent.STM.TVar as X (TVar)

import Data.Bifunctor as X
import Data.Profunctor as X (
    Profunctor(..)
  , Strong(..)
  , Choice(..)
  , Closed(..)
  , Mapping(..)
  , Costrong(..)
  , Cochoice(..)
  )

import Prelude (seq)

liftM' :: Monad m => (a -> b) -> m a -> m b
liftM' = (<$!>)

liftM2' :: Monad m => (a -> b -> c) -> m a -> m b -> m c
liftM2' f a b = do
  x <- a
  y <- b
  let z = f x y
  z `seq` return z
