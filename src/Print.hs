{-# OPTIONS_GHC -Wall #-}
module Print (
  Print(..)
  , putText
  , putLText
  , putTextLn
  , putLTextLn
) where

import Prelude (IO)

import qualified Prelude
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.IO as TL
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BL

class Print a where
  putStr :: a -> IO ()
  putStrLn :: a -> IO ()

instance Print T.Text where
  putStr = T.putStr
  putStrLn = T.putStrLn

instance Print TL.Text where
  putStr = TL.putStr
  putStrLn = TL.putStrLn

instance Print BS.ByteString where
  putStr = BS.putStr
  putStrLn = BS.putStrLn

instance Print BL.ByteString where
  putStr = BL.putStr
  putStrLn = BL.putStrLn

instance Print [Prelude.Char] where
  putStr = Prelude.putStr
  putStrLn = Prelude.putStrLn

-- For forcing type inference
putText :: T.Text -> IO ()
putText = putStr

putLText :: TL.Text -> IO ()
putLText = putStr

putTextLn :: T.Text -> IO ()
putTextLn = putStrLn

putLTextLn :: TL.Text -> IO ()
putLTextLn = putStrLn
