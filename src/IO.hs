{-# OPTIONS_GHC -Wall #-}
module IO (
  module X
) where

import GHC.IO as X (IO, FilePath)
import GHC.IO.Buffer as X (Buffer)
import System.Exit as X
import System.Environment as X (getArgs)
import System.IO as X (Handle, hClose)
