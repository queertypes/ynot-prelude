{-# OPTIONS_GHC -Wall #-}
module GHC (
  module X
) where

import GHC.Enum as X -- Bounded, Enum
import GHC.Num as X
import GHC.Real as X
import GHC.Float as X
import GHC.Show as X
import GHC.Exts as X (Constraint, Ptr, FunPtr, IsString)
