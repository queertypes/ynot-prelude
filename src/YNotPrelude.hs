{-# OPTIONS_GHC -Wall #-}
module YNotPrelude (
  module X,

  curry,
  uncurry,
  tail,
  init,

  undefined,
  error,
  trace,
  traceShow,
  traceShowM,
  traceM,
  traceIO,
  notImplemented,

  identity,
  uncons,
  applyN,
  bool,
  head,
  sortOn,
  ordNub,
  hashNub
) where

import Prelude (curry, uncurry)

import Control as X
import Data as X
import GHC as X
import IO as X
import Path as X
import Print as X
import Safe as X (tailMay, initMay)
import qualified Debug.Trace as T
import qualified Data.List as L
import qualified Data.Hashable as H
import qualified Data.Ord as O
import qualified Data.Set as Set
import qualified Data.HashSet as HashSet
import qualified Prelude as P

tail :: [a] -> Maybe [a]
tail = tailMay

init :: [a] -> Maybe [a]
init = initMay

{-# WARNING undefined "'undefined' remains in code" #-}
undefined :: a
undefined = P.undefined

{-# WARNING error "'error' remains in code" #-}
error :: P.String -> a
error = P.error

{-# WARNING trace "'trace' remains in code" #-}
trace :: P.String -> a -> a
trace = T.trace

{-# WARNING traceShow "'traceShow' remains in code" #-}
traceShow :: P.Show a => a -> a
traceShow a = T.trace (P.show a) a

{-# WARNING traceShowM "'traceShowM' remains in code" #-}
traceShowM :: (P.Show a, P.Monad m) => a -> m ()
traceShowM a = T.traceM (P.show a)

{-# WARNING traceM "'traceM' remains in code" #-}
traceM :: P.Monad m => P.String -> m ()
traceM = T.traceM

{-# WARNING traceIO "'traceIO' remains in code" #-}
traceIO :: P.String -> P.IO ()
traceIO = T.traceIO

{-# WARNING notImplemented "'notImplemented' remains in code" #-}
notImplemented :: a
notImplemented = P.error "Not implemented"

identity :: a -> a
identity x = x

uncons :: [a] -> P.Maybe (a, [a])
uncons []      = P.Nothing
uncons (x:xs)  = P.Just (x, xs)

applyN :: P.Int -> (a -> a) -> a -> a
applyN n f = P.foldr (.) identity (P.replicate n f)

bool :: a -> a -> Bool -> a
bool f t b = if b then t else f

head :: (Foldable f) => f a -> Maybe a
head = foldr (\x _ -> return x) Nothing

sortOn :: (O.Ord o) => (a -> o) -> [a] -> [a]
sortOn = L.sortBy . O.comparing

ordNub :: (O.Ord a) => [a] -> [a]
ordNub = go Set.empty
  where go _ [] = []
        go s (x:xs) =
          if x `Set.member` s
          then go s xs
          else x : go (Set.insert x s) xs

hashNub :: (P.Eq a, H.Hashable a) => [a] -> [a]
hashNub = go HashSet.empty
  where go _ [] = []
        go s (x:xs) =
          if x `HashSet.member` s
          then go s xs
          else x : go (HashSet.insert x s) xs
