# YNot Prelude

| YNot Prelude             | 0.4.2.0                           |
| ------------------------ | --------------------------------- |
| Maintainer               | Allele Dev (allele.dev@gmail.com) |
| Funding                  | $0 USD                            |
| Copyright                | Copyright (C) 2018 Allele Dev     |
| License                  | GPL-3                             |


A prelude borrowing a great deal of ideas from
<http://www.stephendiehl.com/posts/protolude.html>.

Includes at implicit scope:

* Cleaned up Prelude functions (no partial head, etc.)
* Functor, Applicative, Monad modules
* Types for many common container types
* Foldable/Traversable (without partials)
* A Print class for dealing with (L)Text/(L)ByteString/[Char]
* The type synonym String is hidden - must be manually imported
* A subset of lens useful for defining lenses, optics, prisms, optics, and more
  - Just enough operators to use common lenses/prisms
* A subset of IO
* Deepseq
* Async
* STM
* Control.Concurrent
* type-safe file system paths

Because so many names are brought into implicit scope, this may cause
collisions with existing libraries. Use with care.

# License

Uses GPLv3.

# Code of Conduct

Be sure to review the included
[CODE_OF_CONDUCT.md](./CODE_OF_CONDUCT.md), based on the
[Contributor's Covenant](http://contributor-covenant.org/).
